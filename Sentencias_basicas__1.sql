use    [proyecto basico]


select * from dbo.salario

--Sentencias basicas

--Suma: Sumar los valores de una columna espec�fica.
SELECT SUM(salario) FROM dbo.salario;



--Promedio: Calcular el promedio de los valores de una columna espec�fica.

SELECT AVG(salario) FROM dbo.salario;
 

 --M�nimo: Encontrar el valor m�nimo en una columna espec�fica.
 SELECT MIN(salario) FROM dbo.salario;


 --M�ximo: Encontrar el valor m�ximo en una columna espec�fica.
 SELECT MAX(salario) FROM dbo.salario;


--Conteo: Contar el n�mero total de filas en la tabla.

select *FROM dbo.clientes;
SELECT COUNT(*) FROM dbo.clientes;


--Conteo Distinto: Contar el n�mero de valores �nicos en una columna espec�fica.
 
SELECT COUNT(DISTINCT entradas) FROM dbo.cartelera;


--Suma Condicional: Sumar los valores de una columna basados en una condici�n
select *FROM  dbo.libros
SELECT SUM(precio) as Suma_de_precio
FROM dbo.libros
WHERE precio >= 25;



--Promedio Condicional: Calcular el promedio de los valores 

SELECT AVG(salario) AS promedio_salarios 
FROM salario;

select * from salario


-- Salario ordenado de forma ascedente 
SELECT salario
FROM salario
ORDER BY salario ASC;


--Salario ordenado de forma descendente

SELECT salario
FROM salario
ORDER BY salario desc;


-- las pel�culas que comienzan despu�s de las 18:00 horas
SELECT * FROM cartelera 
WHERE hora > '18:00';


--las pel�culas que tienen una entrada con un precio superior a $10


SELECT * FROM cartelera 
WHERE entradas > 10.00;


--las pel�culas que tienen una capacidad igual a la capacidad m�xima de la sala

SELECT * FROM cartelera 
WHERE capacidad = (SELECT MAX(capacidad) FROM cartelera);


--las pel�culas que no son aptas para menores de 12 a�os

SELECT * FROM cartelera 
WHERE sala = 'Sala 2' 
AND entradas BETWEEN 60 AND 100;


--cantidad de peliculas proyectada 
SELECT pelicula, COUNT(*) AS cantidad_peliculas
FROM cartelera
GROUP BY pelicula;


--Todas las pelicula de la cartelera en orden alfabetico
SELECT pelicula, COUNT(*) AS cantidad_peliculas
FROM cartelera
GROUP BY pelicula
ORDER BY pelicula ASC;


--Cantidad de pel�culas por sala:
SELECT sala, COUNT(*) AS cantidad_peliculas
FROM cartelera
GROUP BY sala;


--Cantidad de pel�culas por hora de proyecci�n

SELECT hora, COUNT(*) AS cantidad_peliculas
FROM cartelera
GROUP BY hora;



SELECT * FROM cartelera 

  SELECT pelicula, SUM(capacidad) AS capacidad_total
FROM cartelera
GROUP BY pelicula;


--Promedio del precio de entrada por hora de proyecci�n
SELECT hora, AVG(entradas) AS precio_promedio
FROM cartelera
GROUP BY hora;


--Para obtener los 10 salarios m�s bajos
SELECT TOP 10 salario
FROM salario
ORDER BY salario ASC;




--Para obtener los 10 salarios m�s altos

SELECT TOP 10 salario
FROM salario
ORDER BY salario DESC;


select *from Cartelera

---horas_de_proyeccion de cada pelicula

  

 SELECT pelicula, STUFF((SELECT ', ' + hora
                        FROM cartelera c2
                        WHERE c1.pelicula = c2.pelicula
                        ORDER BY hora
                        FOR XML PATH('')), 1, 2, '') AS horas_de_proyeccion
FROM cartelera c1
GROUP BY pelicula;



--Lista de las pel�culas junto con las horas de proyecci�n, ordenadas alfab�ticamente
SELECT pelicula, hora
FROM cartelera
ORDER BY pelicula, hora;


SELECT c.sala, c.pelicula, c.hora, c.capacidad, c.entradas, r.numero_de_reserva
FROM cartelera c
INNER JOIN reservas r ON c.pelicula = r.pelicula;

create database basico
use  basico
 
 

create table clientes (
idcliente int not null primary key,
nombre varchar(20) not null,
apellido varchar(30) not null,
direccion varchar(100) not null,
ciudad varchar(50) not null,
telefono numeric(10) null,
);

INSERT INTO clientes (idcliente, nombre, apellido, direccion, ciudad, telefono)
VALUES
(1, 'Juan', 'P�rez', 'Calle 123', 'Ciudad A', 1234567890),
(2, 'Mar�a', 'Gonz�lez', 'Avenida 456', 'Ciudad B', 2345678901),
(3, 'Pedro', 'L�pez', 'Calle Principal', 'Ciudad C', 3456789012),
(4, 'Laura', 'Mart�nez', 'Avenida Central', 'Ciudad A', 4567890123),
(5, 'Carlos', 'Hern�ndez', 'Calle 789', 'Ciudad B', 5678901234),
(6, 'Ana', 'S�nchez', 'Avenida Secundaria', 'Ciudad C', 6789012345),
(7, 'Luis', 'Rodr�guez', 'Calle 321', 'Ciudad A', 7890123456),
(8, 'Sof�a', 'Fern�ndez', 'Avenida 654', 'Ciudad B', 8901234567),
(9, 'Andr�s', 'G�mez', 'Calle Secundaria', 'Ciudad C', 9012345678),
(10, 'Marta', 'Torres', 'Avenida Principal', 'Ciudad A', 1234567890),
(11, 'Alejandro', 'Vargas', 'Calle Central', 'Ciudad B', 2345678901),
(12, 'Patricia', 'Ortega', 'Avenida 123', 'Ciudad C', 3456789012),
(13, 'Roberto', 'Jim�nez', 'Calle 456', 'Ciudad A', 4567890123),
(14, 'Elena', 'Ru�z', 'Avenida 789', 'Ciudad B', 5678901234),
(15, 'Javier', 'Navarro', 'Calle Secundaria', 'Ciudad C', 6789012345),
(16, 'Carolina', 'Lara', 'Avenida 321', 'Ciudad A', 7890123456),
(17, 'Diego', 'Silva', 'Calle 654', 'Ciudad B', 8901234567),
(18, 'Luc�a', 'Romero', 'Avenida Central', 'Ciudad C', 9012345678),
(19, 'Gabriel', 'Flores', 'Calle Principal', 'Ciudad A', 1234567890),
(20, 'Valentina', 'Mendoza', 'Avenida Secundaria', 'Ciudad B', 2345678901),
(21, 'Mario', 'L�pez', 'Calle 789', 'Ciudad A', 3456789012),
(22, 'Camila', 'Garc�a', 'Avenida 456', 'Ciudad B', 4567890123),
(23, 'Jos�', 'Hern�ndez', 'Calle Principal', 'Ciudad C', 5678901234),
(24, 'Isabel', 'Rojas', 'Avenida Central', 'Ciudad A', 6789012345),
(25, 'Fernando', 'G�mez', 'Calle 123', 'Ciudad B', 7890123456),
(26, 'Ana', 'Lara', 'Avenida Secundaria', 'Ciudad C', 8901234567),
(27, 'Pedro', 'Fuentes', 'Calle 321', 'Ciudad A', 9012345678),
(28, 'Sara', 'Mart�nez', 'Avenida 654', 'Ciudad B', 1234567890),
(29, 'Gabriel', 'S�nchez', 'Calle Secundaria', 'Ciudad C', 2345678901),
(30, 'Valeria', 'Ortega', 'Avenida Principal', 'Ciudad A', 3456789012),
(31, 'Luisa', 'Vargas', 'Calle Central', 'Ciudad B', 4567890123),
(32, 'Daniel', 'Silva', 'Avenida 123', 'Ciudad C', 5678901234),
(33, 'Carolina', 'Torres', 'Calle 456', 'Ciudad A', 6789012345),
(34, 'Andr�s', 'Guzm�n', 'Avenida 789', 'Ciudad B', 7890123456),
(35, 'Mar�a', 'Romero', 'Calle Secundaria', 'Ciudad C', 8901234567),
(36, 'Alejandro', 'Mendoza', 'Avenida 321', 'Ciudad A', 9012345678),
(37, 'Valentina', 'P�rez', 'Calle 654', 'Ciudad B', 1234567890),
(38, 'Roberto', 'Fern�ndez', 'Avenida Central', 'Ciudad C', 2345678901),
(39, 'Laura', 'Gonz�lez', 'Calle Principal', 'Ciudad A', 3456789012),
(40, 'Javier', 'Soto', 'Avenida Secundaria', 'Ciudad B', 4567890123);

/* borrar antigua tabla de ordenes
   drop table ordenes*/

 


create table ordenes(
id_orden int not null primary key,
idcliente int foreign key references clientes(idcliente),
fecha_orden date default getdate(),
id_vendedor int not null
);

insert into ordenes values(1, 1, '2020-01-12' ,1);
insert into ordenes values(2, 2, '2021-03-20', 2);
insert into ordenes values(3, 3, '2021-06-10', 3);
insert into ordenes values(4, 4, '2021-09-05', 4);
insert into ordenes values(5, 5, GETDATE(),5);
insert into ordenes values(6, 1, '2022-02-28', 1);
insert into ordenes values(7, 2, '2022-05-14', 2);
insert into ordenes values(8, 3, '2022-07-29', 3);
insert into ordenes values(9, 4, GETDATE(), 4);
insert into ordenes values(10, 5, '2022-12-23', 5);
insert into ordenes values(11, 1, '2023-02-14', 1);
insert into ordenes values(12, 2, '2023-04-30', 2);
insert into ordenes values(13, 3, GETDATE(), 3);
insert into ordenes values(14, 4, '2023-09-28', 4);
insert into ordenes values(15, 5, '2023-11-12', 5);
insert into ordenes values(16, 1, '2023-02-05', 1);
insert into ordenes values(17, 2, '2023-04-12', 2);
insert into ordenes values(18, 3, '2023-07-20', 3);
insert into ordenes values(19, 4, GETDATE(), 4);
insert into ordenes values(20, 5, '2023-12-30', 5);
insert into ordenes values(21, 1, '2021-01-15', 1);
insert into ordenes values(22, 2, '2021-03-20', 2);
insert into ordenes values(23, 3, '2021-06-10', 3);
insert into ordenes values(24, 4, '2021-09-05', 4);
insert into ordenes values(25, 5, GETDATE(), 5);
insert into ordenes values(26, 1, '2022-02-28', 1);
insert into ordenes values(27, 2, '2022-05-14', 2);
insert into ordenes values(28, 3, '2022-07-29', 3);
insert into ordenes values(29, 4, GETDATE(), 4);
insert into ordenes values(30, 5, '2022-12-23', 5);
insert into ordenes values(31, 1, '2023-02-14', 1);
insert into ordenes values(32, 2, '2023-04-30', 2);
insert into ordenes values(33, 3, GETDATE(), 3);
insert into ordenes values(34, 4, '2023-09-28', 4);
insert into ordenes values(35, 5, '2023-11-12', 5);
insert into ordenes values(36, 1, '2023-02-05', 1);
insert into ordenes values(37, 2, '2023-04-12', 2);
insert into ordenes values(38, 3, '2023-07-20', 3);
insert into ordenes values(39, 4, GETDATE(), 4);
insert into ordenes values(40, 5, '2023-12-30', 5);




select *from ordenes 
select *from clientes

--Inner join registro que coinciden en ambas tablas

select ordenes.id_orden, clientes.nombre, clientes.apellido
from ordenes
inner join clientes on ordenes.idcliente = clientes.idcliente;



--LEFT OUTER JOIN, devuelve todos los registros de la tabla izquierda (primera) y los
--registros coincidentes de la tabla derecha (segunda)
select *from ordenes 
select *from clientes


select  clientes.nombre, clientes.apellido, ordenes.id_orden
from clientes
left join ordenes on clientes.idcliente = ordenes.idcliente
order by id_orden;


--Selecciona el nombre, apellido, ID y fecha de �rdenes de clientes, mostrando todos 
--los clientes incluso si no tienen �rdenes, y ordena por ID de orden.


select cli.nombre, cli.apellido, ord.id_orden, ord.fecha_orden
from clientes cli
left join ordenes ord on cli.idcliente = ord.idcliente
order by ord.id_orden;


--RIGHT JOIN La palabra clave devuelve todos los registros de la tabla de la derecha(tabla2)
--la palabra clave registros coincidentes de la tabla de la izquierda (Tabla1)

use 
select *from ordenes 
select *from clientes

select cli.nombre, cli.apellido, ord.id_orden, ord.fecha_orden
from clientes cli
right join ordenes ord on cli.idcliente = ord.idcliente
order by ord.id_orden;


